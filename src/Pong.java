import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class Pong extends JFrame {

    private JPanel jContentPane = null;
    private Pong2 painel = null;
    private boolean isApplet = true;

    private Pong2 getPainel() throws Exception {
        if (painel == null) {
        	String ip = "";
			do {
				ip = (String) JOptionPane.showInputDialog(this,
						"Qual é o IP do servidor?",
						"Introduza o ip do servidor: ", JOptionPane.QUESTION_MESSAGE);
				if (ip == null)
					ip = "";
			}while (ip.equals(""));
            painel = new Pong2(this, ip);
        }
        return painel;
    }

    boolean getIsApplet() {return isApplet;}
//Metodo construtor
    public Pong(boolean applet) throws Exception {
        super();
        isApplet = applet;
        //System.out.println(""+isApplet);
        init();

        JMenu file = new JMenu("Menu");
        JMenuItem NovoJogo = new JMenuItem("Novo Jogo");
        file.add(NovoJogo);
        JMenuItem Sair = new JMenuItem("Sair");
        file.add(Sair);
        JMenuBar bar = new JMenuBar();
        setJMenuBar(bar);
        bar.add(file);

        NovoJogo.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        painel.action(e);
                        painel.unico = false;
                        System.out.println("Começou novo jogo!");
                    }
                }
        );

        Sair.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        System.out.println("Fim de jogo");
                        painel.action(e);
                        painel.sair = true;
                    }
                }
        );
        //confirmar se quero mesmo fechar o jogo em execução         
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                painel.sair = true;
            }
        });

// Listener para saber quando se clica numa tecla e se deixa de clicar.
        this.addKeyListener(new KeyAdapter() {

            public void keyPressed(KeyEvent evt) {
                formKeyPressed(evt);
            }
        });
    }

// Metodos que enviam onde clicou e deixou de cliar para a classe Pong2
    private void formKeyPressed(KeyEvent evt) {
        painel.keyPressed(evt);
    }

    private void init() throws Exception {
        this.setTitle("PONG - SADIT");
        this.setResizable(false);
        this.setBounds(new Rectangle(400, 250, 250, 250));
        this.setMaximumSize(new Dimension(500, 350));
        this.setMinimumSize(new Dimension(500, 350));
        this.setContentPane(getJContentPane());
    }

// Metodo q inicializa JContentPanel
    private JPanel getJContentPane() throws Exception {
        if (jContentPane == null) {
            jContentPane = new JPanel();
            jContentPane.setLayout(new BorderLayout());
            jContentPane.add(getPainel(), BorderLayout.CENTER); // bola e jogadores de modo a estarem centrados no panel
        }
        return jContentPane;
    }
}
