import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;

public class Pong2 extends JPanel implements Runnable {
	Pong frame = null;
	boolean pause = false;
	boolean done = false;
	Character character;

	// Definição de variaveis para as posicoes do jogador 1, 2 e da bola.
	private int pbolaX = 250; // Posicao horizontal para a bola
	private int pbolaY = 50; // Posicao vertical para a bola
	private int pjog1X = 10; // Posicao horizontal do jogador 1
	private int pjog1Y = 125; // Posicao vertical do jogador 1
	private int pjog2X = 480; // Posicao horizontal do jogador 2
	private int pjog2Y = 125; // Posicao vertical do jogador 2

	// Direcção de movimento da bola e velocidade da mesma
	private int dir = 3; // vel. direita +
	private int esq = -3; // vel. esquerda -
	private int cima = -1; // vel. cima -
	private int baixo = 1; // vel. baixo +
	private int raq_cima = 5;
	private int raq_baixo = -10;
	private boolean mov_dir = false; // Bola movimenta-se da esq para a direita
	private boolean mov_baixo = true; // Bola no sentido descendente do
										// movimento

	// Velocidade de jogo e bola
	private int velocidade_jogo = 30;

	// Nomes de jogadores e do jogador vencedor
	private String jogador_1 = "1" + '\u00BA' + "Jogador:";
	private String jogador_2 = "2" + '\u00BA' + "Jogador:";
	private String vencedor = "";

	// Pontuacao de cada jogador
	private int resjog1 = 0; // Jogador 1
	private int resjog2 = 0; // Jogador 2

	// Flags de movimento
	private boolean jogador1_cima;
	private boolean jogador1_baixo;
	private boolean jogador2_cima;
	private boolean jogador2_baixo;

	// Modos de jogo
	private boolean jogo;
	// private boolean pausa;
	private boolean fim_de_jogo;
	private boolean ponto;
	protected boolean unico = false;
	protected boolean sair = false;

	// Definicao do tamanho para as raquetes
	private int tam_r1 = 50;
	private int tam_r2 = 50;

	private static int PORT = 8902;
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;

	public Pong2(Pong f, String ip) throws Exception {

		// Setup networking
		//socket = new Socket("193.137.154.204", PORT);
		socket = new Socket("", PORT);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);
		frame = f;
		jogo = true;
		Thread thr1 = new Thread(this);
		thr1.start();
	}

	// Sons
	private String somraquete = "bolaraq.wav";
	private String somparede = "bolapar.wav";
	private String somponto = "ponto.wav";

	// Desenho de bola e raquetes
	public void paintComponent(Graphics gc) {
		setOpaque(true);
		super.paintComponent(gc);
		this.setBackground(Color.black); // cor de "fundo"
		// Bola
		gc.setColor(Color.yellow);
		gc.fillOval(pbolaX, pbolaY, 10, 10);

		// Raquetes
		gc.setColor(Color.white); // cor cinzento
		gc.fillRect(pjog1X, pjog1Y, 10, tam_r1);
		gc.fillRect(pjog2X, pjog2Y, 10, tam_r2);

		// escrever resultado
		Font font = new Font("Arial", Font.PLAIN, 16);
		gc.setFont(font);
		gc.drawString(jogador_1 + " " + resjog1, (this.getWidth() / 4)
				- ((25 / 3) * jogador_1.length()), 295);// apresentar resultado
														// jogador 1
		gc.drawString(jogador_2 + " " + resjog2, (this.getWidth() / 2)
				+ ((25 / 3) * jogador_1.length()), 295);// apresentar resultado
														// jogador 2

		// Apresentar informacao no fim de jogar
		if (fim_de_jogo) {
			gc.drawString("Fim de jogo!", 220, 155);
			gc.drawString("Vencedor: " + vencedor, 205, 175);
		}
	}

	// Representacao da posicao actual da bola
	public void desenharbola(int nx, int ny) {
		pbolaX = nx;
		pbolaY = ny;
		repaint();
	}

	public void action(ActionEvent e) {
		jogo = true;
		// pausa = false;
		fim_de_jogo = false;
		resjog1 = 0;
		resjog2 = 0;
		pbolaX = 250;
		pbolaY = 150;
		pjog1X = 10;
		pjog1Y = 125;
		pjog2X = 475;
		pjog2Y = 125;

		dir = 3;
		esq = -3;
		cima = -1;
		baixo = 1;
		mov_dir = false;
		mov_baixo = true;

		jogador1_cima = false;
		jogador1_baixo = false;
		jogador2_cima = false;
		jogador2_baixo = false;

		tam_r1 = 50;
		tam_r2 = 50;
	}

	// Recepcao e tratar o evento accionado pelo pressionar de uma tecla

	public void keyPressed(KeyEvent evt) {
		String info = "";
		switch (evt.getKeyCode()) {

		// Mover raquete do jogador 1
		case KeyEvent.VK_W:
			info = "MOVE cima;"+getHeight();
			// jogador1_cima = true;
			break;
		case KeyEvent.VK_S:
			info = "MOVE baixo;"+getHeight();
			// jogador1_baixo = true;
			break;
		case KeyEvent.VK_P:
			info = "PAUSE";
			break;
		}
		if (info != "")// caso as teclas pressionadas sejam w ou s ou p
			out.println(info);// escrita para o servidor
	}

	// // Mover raquete do jogador 2
	// case KeyEvent.VK_UP:
	// if (unico == false) {
	// jogador2_cima = true;
	// }
	// break;
	// case KeyEvent.VK_DOWN:
	// if (unico == false) {
	// jogador2_baixo = true;
	// }
	// break;
	// }

	// Recepcao do evento relativo à libertação de uma tecla pressionada
	// public void keyReleased(KeyEvent evt) {
	// //Parar movimento das raquetes pelo anular das flags
	// switch (evt.getKeyCode()) {
	//
	// //Jogador 1
	// case KeyEvent.VK_W:
	// jogador1_cima = false;
	// break;
	// case KeyEvent.VK_S:
	// jogador1_baixo = false;
	// break;
	//
	// //Jogador 2
	// case KeyEvent.VK_UP:
	// if (unico == false) {
	// jogador2_cima = false;
	// }
	// break;
	// case KeyEvent.VK_DOWN:
	// if (unico == false) {
	// jogador2_baixo = false;
	// }
	// break;
	// }
	// }

	// Reproducao de sons
	public void playsound(String sound) {

		try {
			AudioInputStream stream = AudioSystem
					.getAudioInputStream(getClass().getResourceAsStream(sound));
			Clip music = AudioSystem.getClip();
			music.open(stream);
			music.start();
		} catch (LineUnavailableException | UnsupportedAudioFileException
				| IOException e) {
			System.err.println(e.getMessage());
		}
	}

	// Mover raquete do jogador 1
	/*
	 * public void moverJog1() { if (jogador1_cima == true && pjog1Y >= 5) {
	 * pjog1Y += raq_baixo; } if (jogador1_baixo == true && pjog1Y <=
	 * (this.getHeight() - tam_r1)) { pjog1Y += raq_cima; } desenharJog1(pjog1X,
	 * pjog1Y); }
	 * 
	 * // Mover raquete do jogador 2 public void moverJog2() { if (jogador2_cima
	 * == true && pjog2Y >= 5) { pjog2Y += raq_baixo; } if (jogador2_baixo ==
	 * true && pjog2Y <= (this.getHeight() - tam_r2)) { pjog2Y += raq_cima; }
	 * desenharJog2(pjog2X, pjog2Y); }
	 */

	// Redesenho da posicao actual da raquete do jogador1
	public void desenharJog1(int x, int y) {
		this.pjog1X = x;
		this.pjog1Y = y;
		repaint();
	}

	// Redesenho posicao actual da raquete do jogador2
	public void desenharJog2(int x, int y) {
		this.pjog2X = x;
		this.pjog2Y = y;
		repaint();
	}

	// Thread em execucao
	public void run()  {
		while (!done) {

			if (sair == true) {
				int resposta = JOptionPane.showConfirmDialog(frame,
						"Deseja fechar o jogo?", "Atenção",
						JOptionPane.YES_NO_OPTION);
				if (resposta == JOptionPane.YES_OPTION) {
					System.out.println(resposta);
					done = true;
					frame.setVisible(false);
					// System.out.println(""+isApplet);
					if (frame.getIsApplet()) {
						frame.dispose();
					} else {
						System.exit(0);
					}
				} else
					sair = false;
			}

			if (jogo) {
				String response = "";
				try {
					response = in.readLine();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println(response);
				if (response.startsWith("UPDATE")) {
					String[] split = response.substring(7).split(";");
					
					String[] posBola = split[0].split(",");
					pbolaX = Integer.parseInt(posBola[0]);
					pbolaY = Integer.parseInt(posBola[1]);
					
					String[] posPlayer1 = split[1].split(",");
					pjog1X = Integer.parseInt(posPlayer1[0]);
					pjog1Y = Integer.parseInt(posPlayer1[1]);
					
					String[] posPlayer2 = split[2].split(",");
					pjog2X = Integer.parseInt(posPlayer2[0]);
					pjog2Y = Integer.parseInt(posPlayer2[1]);
					
					repaint();
					desenharbola(pbolaX, pbolaY);

					// Metodo para alterar a posição do Jogador1
					desenharJog1(pjog1X, pjog1Y);

					// Metodo para alterar a posição do Jogador2
					desenharJog2(pjog2X, pjog2Y);

					// Bola colide com a raquete do jogador 1
				}
				repaint();
				// Atribuicao de um ponto ao jogador 1
				if (pbolaX >= (492 - 10)) {
					resjog1++;
					ponto = true;
					mov_dir = true;
					//playsound(somponto);
				}

				// Atribuicao de um ponto ao jogador 2
				if (pbolaX <= 10) {
					resjog2++;
					ponto = true;
					mov_dir = false;
					//playsound(somponto);
				}

				// Terminar o jogo, e definir o vencedor
				if (resjog1 == 10 || resjog2 == 10) {

					repaint();

					if (resjog1 > resjog2) {
						vencedor = jogador_1;
					} else {
						vencedor = jogador_2;
					}

					jogo = false;
					// pausa = false;
					fim_de_jogo = true;
					PrintWriter writer;
					try {
						writer = new PrintWriter("resultados.txt", "UTF-8");
						writer.println(resjog1+":"+resjog2+"\n");
						writer.close();
					} catch (FileNotFoundException
							| UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}

			}
		}
	}
}
