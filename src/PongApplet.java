import java.applet.*;

import javax.swing.JFrame;

public class PongApplet extends Applet {
	Pong frame = null;
        boolean isApplet = true;

	public static void main(String args[])
	{
		PongApplet app = new PongApplet();
                app.isApplet = false;
		app.init();
	}

        @Override
	public void init() {
		try {
			frame = new Pong(isApplet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		frame.setVisible(true);
                frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}

}

