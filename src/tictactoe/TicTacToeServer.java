package tictactoe;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Enumeration;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/**
 * A server for a network multi-player tic tac toe game. Modified and extended
 * from the class presented in Deitel and Deitel "Java How to Program" book. I
 * made a bunch of enhancements and rewrote large sections of the code. The main
 * change is instead of passing *data* between the client and server, I made a
 * TTTP (tic tac toe protocol) which is totally plain text, so you can test the
 * game with Telnet (always a good idea.) The strings that are sent in TTTP are:
 * 
 * Client -> Server Server -> Client ---------------- ---------------- MOVE <n>
 * (0 <= n <= 8) WELCOME <char> (char in {X, O}) QUIT VALID_MOVE
 * OTHER_PLAYER_MOVED <n> VICTORY DEFEAT TIE MESSAGE <text>
 * 
 * A second change is that it allows an unlimited number of pairs of players to
 * play.
 */
public class TicTacToeServer {

	/**
	 * Runs the application. Pairs up clients that connect.
	 */
	public static void main(String[] args) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		Game g = new Game();
	}
}

/**
 * A two-player game.
 */
class Game extends JFrame {

	Game frame = null;
	boolean pause = false;
	boolean done = false;
	Player player1;
	Player player2;
	Character character;
	int height = 0;

	// Definição de variaveis para as posicoes do jogador 1, 2 e da bola.
	private int pbolaX = 250; // Posicao horizontal para a bola
	private int pbolaY = 50; // Posicao vertical para a bola

	// Direcção de movimento da bola e velocidade da mesma
	private int dir = 3; // vel. direita +
	private int esq = -3; // vel. esquerda -
	private int cima = -1; // vel. cima -
	private int baixo = 1; // vel. baixo +
	private int raq_cima = 5;
	private int raq_baixo = -10;
	private boolean mov_dir = false; // Bola movimenta-se da esq para a direita
	private boolean mov_baixo = true; // Bola no sentido descendente do
										// movimento

	// Velocidade de jogo e bola
	private int velocidade_jogo = 30;

	// Nomes de jogadores e do jogador vencedor
	private String jogador_1 = "1" + '\u00BA' + "Jogador:";
	private String jogador_2 = "2" + '\u00BA' + "Jogador:";
	private String vencedor = "";

	// Pontuacao de cada jogador
	private int resjog1 = 0; // Jogador 1
	private int resjog2 = 0; // Jogador 2

	// Modos de jogo
	private boolean jogo;
	// private boolean pausa;
	private boolean fim_de_jogo;
	private boolean ponto;
	protected boolean unico = false;
	protected boolean sair = false;

	// Definicao do tamanho para as raquetes
	private int tam_r1 = 50;
	private int tam_r2 = 50;

	public TextArea output = null;

	public void display(String s) {
		output.append(s + "\n");
	}

	class ProcessaJanela extends WindowAdapter {
		public void windowClosing(WindowEvent event) {
			setVisible(false);
			System.exit(0);
		}
	}

	public Game() {
		super();
		jogo = true;
		// Thread thr1 = new Thread(this);
		ServerSocket listener;
		Container cont = getContentPane();
		output = new TextArea(10, 30);
		output.setEditable(false);
		JScrollPane sp = new JScrollPane(output);
		cont.add("Center", sp);
		addWindowListener(new ProcessaJanela());
		setVisible(true);
		pack();
		try {
			listener = new ServerSocket(8902);
			display("Pong Server is Running");
			Enumeration e = NetworkInterface.getNetworkInterfaces();
			while (e.hasMoreElements()) {
				NetworkInterface n = (NetworkInterface) e.nextElement();
				Enumeration ee = n.getInetAddresses();
				while (ee.hasMoreElements()) {
					InetAddress i = (InetAddress) ee.nextElement();
					display(i.getHostAddress());
				}
			}
			try {
				while (true) {
					// inicializamos cada um jogadores do jogo, tendo no
					// construtor
					// o socket de cada um deles.
					Socket s1 = listener.accept();
					this.player1 = this.new Player(s1, 10, 125);
					display("Adicionado player 1");
					Socket s2 = listener.accept();
					this.player2 = this.new Player(s2, 480, 125);
					display("Adicionado player 2");
					// pomos o oponente de cada jogador
					this.player1.setOpponent(this.player2);
					this.player2.setOpponent(this.player1);
					// iniciamos cada jogador
					this.player1.start();
					this.player2.start();
					// iniciamos o jogo
					this.run();
				}
			} catch (IOException e3) {
				System.out.println(e3.getMessage());

			} finally {
				listener.close();
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.exit(0);
		}
		// thr1.start();
	}

	// Representacao da posicao actual da bola
	public void desenharbola(int nx, int ny) {
		pbolaX = nx;
		pbolaY = ny;
	}

	public void action(ActionEvent e) {
		jogo = true;
		// pausa = false;
		fim_de_jogo = false;
		resjog1 = 0;
		resjog2 = 0;
		pbolaX = 250;
		pbolaY = 150;
		player1.x = 10;
		player1.y = 125;
		player2.x = 475;
		player2.y = 125;

		dir = 3;
		esq = -3;
		cima = -1;
		baixo = 1;
		mov_dir = false;
		mov_baixo = true;

		player1.cima = false;
		player1.baixo = false;
		player2.cima = false;
		player2.baixo = false;

		tam_r1 = 50;
		tam_r2 = 50;
	}

	// Mover raquete do jogador 1
	public void moverJog1() {
		if (player1.cima == true && player1.y >= 5) {
			player1.y += raq_baixo;
		}
		if (player1.baixo == true && player1.y <= (height - tam_r1)) {
			player1.y += raq_cima;
		}
		// ponho a false o cima e baixo para que o servidor não pense que as
		// teclas estão sempre pressionadas.
		player1.cima = false;
		player1.baixo = false;

		desenharJog1(player1.x, player1.y);
	}

	// Mover raquete do jogador 2
	public void moverJog2() {
		if (player2.cima == true && player2.y >= 5) {
			player2.y += raq_baixo;
		}
		if (player2.baixo == true && player2.y <= (height - tam_r2)) {
			player2.y += raq_cima;
		}
		// ponho a false o cima e baixo para que o servidor não pense que as
		// teclas estão sempre pressionadas.
		player2.cima = false;
		player2.baixo = false;
		desenharJog2(player2.x, player2.y);
	}

	// Redesenho da posicao actual da raquete do jogador1
	public void desenharJog1(int x, int y) {
		player1.x = x;
		player1.y = y;
		// repaint();
	}

	// Redesenho posicao actual da raquete do jogador2
	public void desenharJog2(int x, int y) {
		player2.x = x;
		player2.y = y;
		// repaint();
	}

	// Thread em execucao
	public void run() {
		while (!done) {

			/*
			 * if (sair == true) { int resposta =
			 * JOptionPane.showConfirmDialog(frame, "Deseja fechar o jogo?",
			 * "Atenção", JOptionPane.YES_NO_OPTION); if (resposta ==
			 * JOptionPane.YES_OPTION) { System.out.println(resposta); done =
			 * true; frame.setVisible(false); //System.out.println(""+isApplet);
			 * if (frame.getIsApplet()) { frame.dispose(); } else {
			 * System.exit(0); } } else sair = false; }
			 */

			if (jogo && !pause) {

				// Caso tenha havido um ponto, bola ao centro:
				if (ponto) {
					pbolaX = 250;
					pbolaY = 50;
					baixo = 1;
					cima = -1;
					dir = 3;
					esq = -3;
					mov_baixo = true;
					ponto = false;
				}

				// Delay que define a velocidade do jogo
				try {
					Thread.sleep(velocidade_jogo);
				} catch (InterruptedException ex) {
				}

				// Definir o movimento horizontal da bola
				if (mov_dir) {
					// Direita
					pbolaX += dir;
					if (pbolaX >= (492 - 10)) {
						mov_dir = false;
					}
				} else {
					// Esquerda
					pbolaX += esq;
					if (pbolaX <= 0) {
						mov_dir = true;
					}
				}

				// Definir o movimento vertical da bola
				if (mov_baixo) {
					// Baixo
					pbolaY += baixo;
					if (pbolaY >= (300 - 10)) {
						mov_baixo = false;
					}

				} else {
					// Cima
					pbolaY += cima;
					if (pbolaY <= 0) {
						mov_baixo = true;
					}
				}
				atualizarClientes();

				// Bola colide com a raquete do jogador 1
				if (pbolaX <= (player1.x + 10) && pbolaY >= player1.y
						&& pbolaY <= (player1.y + (tam_r1 / 5))) {
					mov_dir = true;
					mov_baixo = false;

					if (dir < 8) {
						dir++;
						esq--;
					}

					if (dir > 8) {
						dir--;
						esq++;
					}

					if (baixo < 9) {
						cima--;
						baixo++;
					}
				}

				if (pbolaX <= (player1.x + 10)
						&& pbolaY > (player1.y + tam_r1 / 5)
						&& pbolaY < (player1.y + (3 * tam_r1 / 5))) {
					mov_dir = true;

					if (dir < 8) {
						dir++;
						esq--;
					}

					if (dir > 8) {
						dir--;
						esq++;
					}

					if (baixo > 9) {
						cima++;
						baixo--;
					}
				}

				if (pbolaX <= (player1.x + 10)
						&& pbolaY >= (player1.y + (3 * tam_r1 / 5))
						&& pbolaY <= (player1.y + tam_r1)) {
					mov_dir = true;
					mov_baixo = true;

					if (dir < 8) {
						dir++;
						esq--;
					}

					if (dir > 8) {
						dir--;
						esq++;
					}

					if (baixo < 9) {
						cima--;
						baixo++;
					}

				}

				// Bola colide com a raquete do jogador 2
				if (pbolaX >= (player2.x - 10) && pbolaY >= player2.y
						&& pbolaY <= (player2.y + (tam_r2 / 5))) {
					mov_dir = false;
					mov_baixo = false;

					if (dir < 8) {
						dir++;
						esq--;
					}

					if (dir > 8) {
						dir--;
						esq++;
					}

					if (baixo < 9) {
						cima--;
						baixo++;
					}
				}

				if (pbolaX >= (player2.x - 10)
						&& pbolaY > (player2.y + tam_r2 / 5)
						&& pbolaY < (player2.y + (3 * tam_r2 / 5))) {
					mov_dir = false;

					if (dir < 8) {
						dir++;
						esq--;
					}

					if (dir > 8) {
						dir--;
						esq++;
					}

					if (baixo > 1) {
						cima++;
						baixo--;
					}
				}

				if (pbolaX >= (player2.x - 10)
						&& pbolaY >= (player2.y + (3 * tam_r2 / 5))
						&& pbolaY <= (player2.y + tam_r2)) {
					mov_dir = false;
					mov_baixo = true;

					if (dir < 8) {
						dir++;
						esq--;
					}

					if (dir > 8) {
						dir--;
						esq++;
					}

					if (baixo < 9) {
						cima--;
						baixo++;
					}
				}

				// Atribuicao de um ponto ao jogador 1
				if (pbolaX >= (492 - 10)) {
					resjog1++;
					ponto = true;
					mov_dir = true;
				}

				// Atribuicao de um ponto ao jogador 2
				if (pbolaX <= 10) {
					resjog2++;
					ponto = true;
					mov_dir = false;
				}

				// Terminar o jogo, e definir o vencedor
				if (resjog1 == 10 || resjog2 == 10) {

					if (resjog1 > resjog2) {
						vencedor = jogador_1;
					} else {
						vencedor = jogador_2;
					}

					jogo = false;
					// pausa = false;
					fim_de_jogo = true;
				}

			}
		}
	}

	public void atualizarClientes() {
		desenharbola(pbolaX, pbolaY);

		// Metodo para alterar a posição do Jogador1
		moverJog1();

		// Metodo para alterar a posição do Jogador2
		moverJog2();

		// criamos a string de informação que vamos enviar para os clientes,
		// para atualizar os jogos
		String info = "UPDATE " + pbolaX + "," + pbolaY + ";" + player1.x + ","
				+ player1.y + ";" + player2.x + "," + player2.y;
		// System.out.println(info);
		// enviamos a informação para cada jogador
		player1.output.println(info);
		player2.output.println(info);
	}

	/**
	 * The class for the helper threads in this multithreaded server
	 * application. A Player is identified by a character mark which is either
	 * 'X' or 'O'. For communication with the client the player has a socket
	 * with its input and output streams. Since only text is being communicated
	 * we use a reader and a writer.
	 */
	class Player extends Thread {
		char mark;
		Player opponent;
		Socket socket;
		BufferedReader input;
		PrintWriter output;
		int x;
		int y;
		boolean cima;
		boolean baixo;

		/**
		 * Constructs a handler thread for a given socket and mark initializes
		 * the stream fields, displays the first two welcoming messages.
		 */
		public Player(Socket socket, int x, int y) {
			this.socket = socket;
			this.x = x;
			this.y = y;
			try {
				input = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				output = new PrintWriter(socket.getOutputStream(), true);
				output.println("WELCOME");
				output.println("MESSAGE Waiting for opponent to connect");
			} catch (IOException e) {
				System.out.println("Player died: " + e);
			}
		}

		/**
		 * Accepts notification of who the opponent is.
		 */
		public void setOpponent(Player opponent) {
			this.opponent = opponent;
		}

		/**
		 * The run method of this thread.
		 */
		public void run() {
			try {
				// The thread is only started after everyone connects.
				output.println("MESSAGE All players connected");

				// Tell the first player that it is her turn.
				if (mark == 'X') {
					output.println("MESSAGE Your move");
				}

				// Repeatedly get commands from the client and process them.
				while (true) {
					String command = input.readLine();
					if (command.startsWith("MOVE")) {
						// substring - retorna os caractéres a partir da posição
						// 5 de command.
						String temp = command.substring(5);
						// corta a string em partes, (p.ex. 1;2;3 em array com 3
						// posições com [0]="1"...)
						String[] s = temp.split(";");
						// na posição 0 tem a direcção
						String dir = s[0];
						// na posição 1 temos o height; parseInt faz conversão
						// de string em int
						height = Integer.parseInt(s[1]);
						// em vez de aceder à antiga variável playerx_cima,
						// acedes diretamente à variável cima daquele
						// determinado player.
						if (dir.equals("cima"))
							cima = true;
						else
							// baixo
							baixo = true;
					} else if (command.startsWith("QUIT")) {
						return;
					} else if (command.startsWith("PAUSE")) {
						if (Game.this.pause)
							Game.this.pause = false;
						else
							Game.this.pause = true;
					}
				}
			} catch (IOException e) {
				System.out.println("Player died: " + e);
			} finally {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}
}